class CourseDB {
    db;
    constructor() {
        if (sessionStorage.getItem("courseDB") === null)
            sessionStorage.setItem("courseDB", JSON.stringify(defaultCourseDB));

        self.db = JSON.parse(sessionStorage.getItem("courseDB"));
    }

    // kurs yoksa, kaydet
    getCourseColor(courseId) {
        let defaultColor = "#e66465";

        if (! db.hasOwnProperty(courseId)) {
            db[courseId] = [courseId, defaultColor, 0];
        }

        return db[courseId][1];
    }

    setCourseColor(courseId, newColor) {
        db[courseId][1] = newColor;
    }

    getCourseAkts(courseId) {

        return db[courseId][2];
    }

    setCourseAkts(courseId, newAkts) {
        db[courseId][2] = newAkts;
    }

    getCourseName(courseId) {
        if (db.hasOwnProperty(courseId))
            return db[courseId][0];
        return ""
    }

    save() {
        sessionStorage.setItem("courseDB", JSON.stringify(db));
    }


}


defaultCourseDB = {

    "BM101": ["BM101 BİLGİSAYAR PROGRAMLAMA I", "#e66465", 5],
    "BM103": ["BİLGİSAYAR MÜHENDİSLİĞİNE GİRİŞ", "#e66465", 4],
    "ENG103": ["ENGLISH I", "#252341", 3],
    "FIZ103": ["FIZIK I", "#a51d2d", 6],
    "MATH101": ["MATHEMATICS I", "#3584e4", 6],
    "MAT199": ["LİNEER CEBİR", "#3f3fc4", 4],
    "TAR-101": ["ATATÜRK İLKELERİ ve İNKILAP TARİHİ-I", "#233f00", 2],
    "BM207": ["OLASILIK VE İSTATİSTİK", "#b09c3b", 4],
    "BM211": ["ELEKTRİK VE ELEKTRONİK DEVRELER", "#752828", 5],
    "CENG205": ["DATA STRUCTURES", "#e66465", 6],
    "CENG213": ["OBJECT ORIENTED PROGRAMMING", "#e66465", 6],
    "ENG203": ["ACADEMIC ENGLISH I", "#2c2660", 3],
    "MATH201": ["DIFFERENTIAL EQUATIONS", "#1a5fb4", 5],
    "TÜR-101": ["TÜRK DİLİ-I", "#b20000", 2],
    "BM301": ["STAJ I", "#e66465", 2],
    "BM309": ["İŞLETİM SİSTEMLERİ", "#1e8254", 6],
    "BM311": ["BİLGİSAYAR MİMARİSİ", "#e66465", 6],
    "BM315": ["MÜHENDİSLİK PROJESİ", "#e66465", 2],
    "BM351": ["ROBOTİK (TEK. SEÇ.)", "#e66465", 6],
    "BM353": ["HABERLEŞME TEMELLERİ (TEK. SEÇ.)", "#e66465", 6],
    "BM357": ["ELEKTRONİK TİCARETİN TEMELLERİ (TEK. SEÇ.)", "#e66465", 6],
    "BM359": ["İNTERNET PROGRAMLAMA (TEK. SEÇ.)", "#e66465", 6],
    "BM361": ["NESNE YÖNELİMLİ ANALİZ VE TASARIM (TEK. SEÇ.)", "#e66465", 6],
    "BM363": ["WEB TABANLI TEKNOLOJİLER (TEK. SEÇ.)", "#e66465", 6],
    "BM365": ["SİNYALLER VE SİSTEMLER (TEK. SEÇ.)", "#e66465", 6],
    "BM367": ["BETİK DİLLER (TEK. SEÇ.)", "#e66465", 6],
    "BM381": ["HUKUKUN TEMEL KAVRAMLARI (SOS.SEÇ)", "#e66465", 3],
    "BM383": ["İKTİSADA GİRİŞ (SOS.SEÇ)", "#e66465", 3],
    "BM385": ["UYGARLIK TARİHİ (SOS.SEÇ)", "#e66465", 3],
    "BM387": ["BİLİMSEL ARAŞTIRMA TEKNİKLERİ (SOS.SEÇ)", "#e66465", 3],
    "BM389": ["SOSYAL PSİKOLOJİ (SOS.SEÇ)", "#e66465", 3],
    "CENG307": ["FILE ORGANIZATION", "#4c8d00", 4],
    "CENG313": ["INTRODUCTION TO DATA SCIENCE", "#e66465", 4],
    "CENG351": ["ROBOTICS (TECH. ELECT.)", "#e66465", 6],
    "CENG353": ["FUNDAMENTALS OF COMMUNICATION (TECH. ELECT.)", "#e66465", 6],
    "CENG357": ["FUNDAMENTALS OF ELECTRONIC COMMERCE (TECH. ELECT.)", "#e66465", 6],
    "CENG359": ["INTERNET PROGRAMMING (TECH. ELECT.)", "#e66465", 6],
    "CENG361": ["OBJECT ORIENTED ANALYSIS AND DESIGN (TECH. ELECT.)", "#e66465", 6],
    "CENG363": ["WEB BASED TECHNOLOGIES (TECH. ELECT.)", "#e66465", 6],
    "CENG365": ["SIGNALS AND SYSTEMS (TECH. ELECT.)", "#e66465", 6],
    "CENG367": ["SCRIPTING LANGUAGES (TECH. ELECT.)", "#e66465", 6],
    "BM401": ["STAJ II", "#e66465", 2],
    "BM403": ["VERİ İLETİŞİMİ", "#e66465", 3],
    "BM451": ["İLERİ BİLGİSAYAR MİMARİSİ (TEK. SEÇ.)", "#e66465", 6],
    "BM453": ["GERÇEK ZAMAN SİSTEMLERİ (TEK. SEÇ.)", "#e66465", 6],
    "BM455": ["YAPAY ZEKAYA GİRİŞ (TEK. SEÇ.)", "#e66465", 6],
    "BM459": ["BİLGİSAYAR MÜHENDİSLİĞİNDE ÖZEL KONULAR I (TEK. SE", "#e66465", 6],
    "BM461": ["BİYOİNFORMATİK (TEK. SEÇ.)", "#e66465", 6],
    "BM463": ["COĞRAFİ BİLGİ SİSTEMLERİ (TEK. SEÇ.)", "#e66465", 6],
    "BM465": ["DAĞITIK SİSTEMLER (TEK. SEÇ.)", "#e66465", 6],
    "BM469": ["GENETİK ALGORİTMALAR VE PROGRAMLAMA (TEK. SEÇ.)", "#e66465", 6],
    "BM471": ["GÖRÜNTÜ İŞLEMEYE GİRİŞ (TEK. SEÇ.)", "#e66465", 6],
    "BM473": ["KABLOSUZ VE MOBİL AĞLARA GİRİŞ (TEK. SEÇ.)", "#e66465", 6],
    "BM475": ["KRİPTOGRAFİYE GİRİŞ (TEK. SEÇ.)", "#e66465", 6],
    "BM477": ["MİKRODENETLEYİCİLER (TEK. SEÇ.)", "#810002", 6],
    "BM479": ["PARALEL BİLGİSAYAR MİMARİLERİ VE PROGRAMLAMA (TEK.", "#e66465", 6],
    "BM481": ["SİSTEM MÜHENDİSLİĞİ (TEK. SEÇ.)", "#e66465", 6],
    "BM483": ["TASARIM ÖRÜNTÜLERİ (TEK. SEÇ.)", "#e66465", 6],
    "BM485": ["UZAKTAN EĞİTİM TEKNOLOJİLERİ (TEK. SEÇ.)", "#e66465", 6],
    "BM487": ["YÖNETİM BİLİŞİM SİSTEMLERİ (TEK. SEÇ.)", "#e66465", 6],
    "BM489": ["VERİ MADENCİLİĞİ (TEK. SEÇ.)", "#0e0303", 6],
    "BM491": ["SİSTEM PROGRAMLAMA (TEK. SEÇ.)", "#e66465", 6],
    "BM493": ["VERİ MAHREMİYETİNE GİRİŞ (TEK. SEÇ.)", "#e66465", 6],
    "BM495": ["BİLGİSAYAR PROJESİ I", "#e66465", 5],
    "BM497": ["GÖMÜLÜ SİSTEMLER (TEK. SEÇ.)", "#e66465", 6],
    "BM499": ["SİSTEM BENZETİMİ (TEK. SEÇ.)", "#e66465", 6],
    "CENG451": ["ADVANCED COMPUTER ARCHITECTURE (TECH. ELECT.)", "#e66465", 6],
    "CENG453": ["REAL TIME SYSTEMS (TECH. ELECT.)", "#e66465", 6],
    "CENG455": ["INTRODUCTION TO ARTIFICIAL INTELLIGENCE (TECH. ELE", "#e66465", 6],
    "CENG459": ["SPECIAL TOPICS IN COMPUTER ENGINEERING I (TECH. EL", "#e66465", 6],
    "CENG461": ["BIOINFORMATICS (TECH. ELECT.)", "#e66465", 6],
    "CENG463": ["GEOGRAPHIC INFORMATION SYSTEMS (TECH. ELECT.)", "#e66465", 6],
    "CENG465": ["DISTRIBUTED SYSTEMS (TECH. ELECT.)", "#e66465", 6],
    "CENG467": ["INFORMATION THEORY (TECH. ELECT.)", "#e66465", 6],
    "CENG469": ["GENETIC ALGORITHMS AND PROGRAMMING (TECH. ELECT.)", "#e66465", 6],
    "CENG471": ["INTRODUCTION TO IMAGE PROCESSING (TECH. ELECT.)", "#e66465", 6],
    "CENG473": ["INTRODUCTION TO WIRELESS AND MOBILE NETWORKS (TECH", "#e66465", 6],
    "CENG475": ["INTRODUCTION TO CRYPTOGRAPHY (TECH. ELECT.)", "#e66465", 6],
    "CENG477": ["MICROCONTROLLERS (TECH. ELECT.)/td>", "#e66465", 6],
    "CENG479": ["PARALLEL COMPUTER ARCHITECTURES AND PROGRAMMING (T", "#e66465", 6],
    "CENG481": ["SYSTEM ENGINEERING (TECH. ELECT.)", "#e66465", 6],
    "CENG483": ["DESIGN PATTERNS (TECH. ELECT.)", "#e66465", 6],
    "CENG485": ["DISTANCE LEARNING TECHNOLOGIES (TECH. ELECT.)", "#e66465", 6],
    "CENG487": ["MANAGEMENT INFORMATION SYSTEMS (TECH. ELECT.)", "#e66465", 6],
    "CENG489": ["DATA MINING (TECH. ELECT.)", "#0e0303", 6],
    "CENG491": ["SYSTEM PROGRAMMING (TECH. ELECT.)", "#e66465", 6],
    "CENG493": ["INTRODUCTION TO DATA PRIVACY (TECH. ELECT.)", "#e66465", 6],
    "CENG497": ["EMBEDDED SYSTEMS (TECH. ELECT.)", "#e66465", 6],
    "CENG499": ["SYSTEM SIMULATION (TECH. ELECT.)", "#e66465", 6],
    "ISG401": ["İŞ SAĞLIĞI VE GÜVENLİĞİ I", "#e66465", 2],
    "BM102": ["BİLGİSAYAR PROGRAMLAMA II", "#567e14", 5],
    "BM104": ["AYRIK MATEMATİK", "#2f2ffa", 3],
    "BM181": ["MÜZİK TARİHİ (TOS)", "#a920ce", 3],
    "BM182": ["ENDÜSTRİYEL PSİKOLOJİ (TOS)", "#e66465", 3],
    "BM183": ["DAVRANIŞ BİLİMLERİ (TOS)", "#e66465", 3],
    "BM184": ["SANAT TARİHİ (TOS)", "#e66465", 3],
    "BM185": ["HALKLA İLİŞKİLER (TOS)", "#e66465", 3],
    "BM186": ["GENEL MÜZİK TARİHİ (TOS)", "#66007c", 3],
    "BM187": ["TÜRK MÜZİK TARİHİ (TOS)", "#e66465", 3],
    "BM188": ["İŞLETME EKONOMİSİ VE YÖNETİMİ (TOS)", "#e66465", 3],
    "BM189": ["TRAFİK İLK YARDIMI (TOS)", "#e66465", 3],
    "BM191": ["İŞ HUKUKU (TOS)", "#e66465", 3],
    "BM192": ["BESİN KİMYASI (TOS)", "#30054f", 3],
    "ENG104": ["ENGLISH II", "#e66465", 3],
    "FIZ156": ["FİZİK LABORATUVARI", "#933f3f", 2],
    "MAT102": ["MATEMATİK II", "#000054", 6],
    "PHYS104": ["PHYSICS II", "#670102", 6],
    "TAR-102": ["ATATÜRK İLKELERİ VE İNKILAP TARİHİ-II", "#e66465", 2],
    "BM218": ["ALGORİTMALAR", "#9f0b0e", 6],
    "BM222": ["SAYISAL TASARIM", "#0a0a8a", 6],
    "BM224": ["PROGRAMLAMA DİLLERİ", "#c74646", 6],
    "CENG206": ["NUMERICAL ANALYSIS", "#242a96", 4],
    "ENG204": ["ACADEMIC ENGLISH II", "#16132c", 3],
    "TÜR-102": ["TÜRK DİLİ-II", "#b70909", 2],
    "BM312": ["BİÇİMSEL DİLLER VE OTOMATLAR", "#e66465", 6],
    "BM314": ["YAZILIM MÜHENDİSLİĞİ", "#e66465", 6],
    "BM352": ["MATEMATİKSEL MODELLEME (TEK. SEÇ.)", "#101036", 6],
    "BM354": ["UZMAN SİSTEMLER (TEK. SEÇ.)", "#e66465", 6],
    "BM356": ["ÇEVİRİCİ DİLLER (TEK. SEÇ.)", "#e66465", 6],
    "BM358": ["GRAF TEORİSİ (TEK. SEÇ.)", "#e66465", 6],
    "BM364": ["VERİTABANI UYGULAMALARI (TEK. SEÇ.)", "#e66465", 6],
    "BM366": ["SİSTEM ANALİZİ (TEK. SEÇ.)", "#e66465", 6],
    "BM368": ["ALGORİTMA ANALİZİ VE TASARIMI (TEK. SEÇ.)", "#e66465", 6],
    "BM372": ["JAVA PROGRAMLAMA (TEK. SEÇ.)", "#e3d4d4", 6],
    "BM380": ["İŞLETME BİLİMİNE GİRİŞ (SOS.SEÇ)/td>", "#e66465", 3],
    "BM382": ["PAZARLAMAYA GİRİŞ (SOS.SEÇ)", "#e66465", 3],
    "BM384": ["SOSYOLOJİ (SOS.SEÇ)", "#e66465", 3],
    "BM386": ["YÖNETİM BİLİMİ (SOS.SEÇ)", "#e66465", 3],
    "BM388": ["SOSYAL POLİTİKA (SOS.SEÇ)", "#e66465", 3],
    "BM390": ["HUKUK BAŞLANGICI (SOS.SEÇ)", "#e66465", 3],
    "BM392": ["GİRİŞİMCİLİK VE İNOVASYON (SOS.SEÇ.)", "#e66465", 3],
    "CENG316": ["DATABASE SYSTEMS", "#e66465", 6],
    "CENG318": ["MICROPROCESSORS", "#e66465", 6],
    "CENG352": ["MATHEMATICAL MODELING (TECH. ELECT.)", "#455fb2", 6],
    "CENG354": ["EXPERT SYSTEMS (TECH. ELECT.)", "#e66465", 6],
    "CENG356": ["ASSEMBLY LANGUAGES (TECH. ELECT.)", "#e66465", 6],
    "CENG358": ["GRAPH THEORY (TECH. ELECT.)", "#e66465", 6],
    "CENG364": ["DATABASE APPLICATIONS (TECH. ELECT.)", "#e66465", 6],
    "CENG366": ["SYSTEM ANALYSIS (TECH. ELECT.)", "#e66465", 6],
    "CENG368": ["ALGORITHM ANALYSIS AND DESIGN (TECH. ELECT.)", "#e66465", 6],
    "CENG372": ["JAVA PROGRAMMING (TECH. ELECT.)", "#e66465", 6],
    "BM402": ["BİLGİSAYAR AĞLARI", "#e66465", 4],
    "BM452": ["SAYISAL SİNYAL İŞLEME (TEK. SEÇ.)", "#00008d", 6],
    "BM458": ["DERLEYİCİLER VE KOD ÜRETİMİ (TEK. SEÇ.)", "#e66465", 6],
    "BM462": ["BULANIK MANTIK (TEK. SEÇ.)", "#e66465", 6],
    "BM464": ["ÇOKLUORTAM SİSTEMLERİ (TEK. SEÇ.)", "#e66465", 6],
    "BM466": ["PERSEPTRON AĞLAR VE UYGULAMALARI (TEK. SEÇ.)", "#e66465", 6],
    "BM468": ["E-İMZA VE AÇIK ANAHTAR ALTYAPISI (TEK. SEÇ.)", "#e66465", 6],
    "BM472": ["GÜVENLİ KODLAMA (TEK. SEÇ.)", "#e66465", 6],
    "BM474": ["KONTROL SİSTEMLERİ (TEK. SEÇ.)", "#e66465", 6],
    "BM476": ["MAKİNA ÖĞRENMESİNE GİRİŞ (TEK. SEÇ.)", "#2a1818", 6],
    "BM478": ["NANOTEKNOLOJİLER (TEK. SEÇ.)", "#e66465", 6],
    "BM482": ["BİLGİSAYAR GÜVENLİĞİNE GİRİŞ (TEK. SEÇ.)", "#e66465", 6],
    "BM484": ["UNIX PROGRAMLAMA (TEK. SEÇ.)", "#e66465", 6],
    "BM486": ["VLSI TASARIMI (TEK. SEÇ.)", "#e66465", 6],
    "BM488": ["YÖNEYLEM ARAŞTIRMASI (TEK. SEÇ.)", "#e66465", 6],
    "BM492": ["BİLGİSAYARLA GRAFİK (TEK. SEÇ.)", "#e66465", 6],
    "BM494": ["BİLGİSAYAR MÜHENDİSLİĞİNDE ÖZEL KONULAR II (TEK. S", "#e66465", 6],
    "BM496": ["BİLGİSAYAR PROJESİ II", "#e66465", 6],
    "BM498": ["AÇIK KAYNAK KODLAMA (TEK. SEÇ.)", "#e66465", 6],
    "CENG452": ["DIGITAL SIGNAL PROCESSING (TECH. ELECT.)", "#e66465", 6],
    "CENG458": ["COMPILERS AND CODE GENERATION (TECH. ELECT.)", "#e66465", 6],
    "CENG462": ["FUZZY LOGIC (TECH. ELECT.)", "#e66465", 6],
    "CENG464": ["MULTIMEDIA SYSTEMS (TECH. ELECT.)", "#e66465", 6],
    "CENG466": ["PERCEPTRON NETWORKS AND APPLICATIONS (TECH. ELECT.", "#e66465", 6],
    "CENG468": ["E-SIGNATURE AND PUBLIC KEY INFRASTRUCTURES (TECH.", "#e66465", 6],
    "CENG472": ["SECURE CODING (TECH. ELECT.)", "#e66465", 6],
    "CENG474": ["CONTROL SYSTEMS (TECH. ELECT.)", "#e66465", 6],
    "CENG476": ["INTRODUCTION TO MACHINE LEARNING (TECH. ELECT.)", "#e66465", 6],
    "CENG478": ["NANOTECHNOLOGIES (TECH. ELECT.)", "#e66465", 6],
    "CENG482": ["INTRODUCTION TO COMPUTER SECURITY (TECH. ELECT.)", "#e66465", 6],
    "CENG484": ["UNIX PROGRAMMING (TECH. ELECT.)", "#e66465", 6],
    "CENG486": ["VLSI DESIGN (TECH. ELECT.)", "#e66465", 6],
    "CENG488": ["OPERATIONS RESEARCH (TECH. ELECT.)", "#e66465", 6],
    "CENG492": ["COMPUTER GRAPHICS (TECH. ELECT.)", "#e66465", 6],
    "CENG494": ["SPECIAL TOPICS IN COMPUTER ENGINEERING II (TECH. E", "#e66465", 6],
    "CENG498": ["OPEN SOURCE CODING (TECH. ELECT.)", "#e66465", 6],
    "ISG402": ["İŞ SAĞLIĞI VE GÜVENLİĞİ II", "#e66465", 2],
}
