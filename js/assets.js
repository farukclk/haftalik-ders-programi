
class Matrix {
    constructor() {

    }

    
    getTimeId(text) {
        if (text.includes("08:3"))
            return 0;
        else if (text.includes("09:3"))
            return 1;
        else if (text.includes("10:3"))
            return 2;
        else if (text.includes("11:3"))
            return 3;
        else if (text.includes("12:3"))
            return 4;
        else if (text.includes("13:3"))
            return 5;
        else if (text.includes("14:3"))
            return 6;
        else if (text.includes("15:3"))
            return 7;
        else if (text.includes("16:3"))
            return 8;
        else if (text.includes("17:3"))
            return 9;
        else if (text.includes("18:3"))
            return 10;
        else if (text.includes("19:3"))
            return 11;
        else if (text.includes("20:3"))
            return 12;
        else if (text.includes("21:3"))
            return 13;
        else {
            console.log("Hata [!] : Uyumsuz ders saati  " + this.getCourseName(text));
        }
    }


    // 0=pzt, 1=sali, 2=car, 3=per, 4=cuma
     getDayId(line, day) {
        if (line.includes("Pazartesi"))
            return 0;
        else if (line.includes("Cuma"))
            return 4;
        else
            return day;

    }


// 0=(pzt or cuma), 1=sali, 2=car, 3=per

    getPart(line, day) {
        if (day === 0 || day === 4)
            return line.slice(4, 32);
        else if (day === 1)
            return line.slice(35, 63);
        else if (day === 2)
            return line.slice(66, 94);
        else if (day === 3)
            return line.slice(97, 125);

    }


// return course name

    getCourseName(text) {

        let name = ""
        let line = ""

        text.split("\n").forEach(i => {
            line = i.slice(2, 9);
            if (line.trim() !== "")
                name += line.trim();
        });

        return name;
    }



    getBranch(text) {
        let branch = "";

        text.split("\n").forEach((line) => {
            try {
                if (line.charAt(0).match(/\d/)) {
                    branch += line.charAt(0);
                }
            } catch (error) {

            }
        });
        return branch;
    }


    set_all_course_name(text, list) {

      console.log(list);
      console.log("-----");

        let lines = text.split("\n");
        let data = "";

        for (let part = 0; part < 4; part++) {


            lines.forEach(line => {
                line =this.getPart(line, part);

                if (line.slice(18) === "──────────") {
                    if (data.includes(":")) {
                        if (!list.includes(this.getCourseName(data)))
                            list.push(this.getCourseName(data));
                    }
                    data = "";
                } else
                    data += line + "\n";

            });
        }
    }

    createMatrix(text, dersler) {


        let lines = text.split("\n");
        let day = "";
        let data = "";


        for (let part = 0; part < 4; part++) {
            day = part;

            lines.forEach(line => {
                line =this.getPart(line, part);

                day = this.getDayId(line, day); // update day cuma yda pzt

                if (line.slice(18) === "──────────") {
                    if (data.includes(":")) {
                        try {
                            dersler[this.getTimeId(data) * 5 + day].push(this.getCourseName(data) + "_" + this.getBranch(data));
                        }
                        catch (error) {
                            console.log(data);
//                            console.log(this.getCoursedata));
                            console.log(this.getCourseName(data));
                            console.log(this.getBranch(data));
                        }

                    }
                    data = "";
                } else
                    data += line + "\n";

            });
        }
    }
}

