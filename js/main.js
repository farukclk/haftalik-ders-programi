let courseTable = JSON.parse(sessionStorage.getItem("courseTable"));

let ders_isimleri = JSON.parse(sessionStorage.selected_courses);
let selectedCourseNames = [];


//sube ismleriyle
let secilen_dersler = [];
let secilmeyen_dersler = [];
const saatler = Array("8:30-9:20", "9:30-10:20", "10:30 - 11:20", "11:30 - 12:20", "12:30 - 13:20", "13:30 - 14:20", "14:30 - 15:20", "15:30 - 16:20", "16:30 - 17:20", "17:30 - 18:20", "18:30 - 19:20", "19:30 - 20:20", "20:30 - 21:20", "21:30 - 22:20");

let allCourses = []; // subeleriyle birlikte tum dersler
let name;
for (let box of courseTable) {
    for (let course of box) {
        if (ders_isimleri.includes(getCourseId(course)) && !allCourses.includes(course))
            allCourses.push(course);
    }
}


let courseDB = new CourseDB();


let tmp_list = [];

// courseTable daki ilgi dısı dersleri kaldır
for (let i = 0; i < courseTable.length; i++) {

    tmp_list = [];
    courseTable[i].forEach(ii => {
        if (ders_isimleri.includes(getCourseId(ii))) {
            tmp_list.push(ii);
        }
    })
    courseTable[i] = tmp_list;
}


index();
rightSide();


function rightSide() {
    let div = document.getElementById("rightSide");
    let data = "<ul>";
    let courses = JSON.parse(sessionStorage.selected_courses);


    courses.forEach(course => {
        if (!selectedCourseNames.includes(course)) {
            data += "<li>" + course + "</li>"
        }
    });

    data += "</ul>"
    div.innerHTML = data;

}


function getCourseId(course) {
    return course.split("_")[0];
}


function index() {
    let id = 0;
    let text = "<table cellspacing='2' cellpadding='1'>";
    text += "<tr class='days' ><td><a class='geriDon' href='./index.html'>Anasayfa</a></td><td class='days'>Pazartesi</td> <td class='days'>Salı</td> <td class='days'>Çarşamba</td> <td class='days'>Perşembe</td> <td class='days'>Cuma</td>";

    for (let i = 0; i <= getLastCourseFinishTime(); i++) {
        let td = "<td class='time'>" + saatler[i] + "</td>";
        for (let day = 0; day < 5; day++) {

            td += "<td id='" + id++ + "'>" + "<select class='mySelect' " + "style='visibility:visible'></select>" + "</td>";

        }
        text += "<tr>" + td + "</tr>";
    }
    text += "</table>";

    document.getElementById("table").innerHTML = text;


    tum_optionlari_yenile();
    cakismasizDersleriSec();
    rightSide();

}


//Return timeId as y-axis from 0 to 13
function getLastCourseFinishTime() {

    for (let x = 13; x >= 0; x--) {
        for (let id = x * 5; id < x * 5 + 5; id++) {
            if (courseTable[id].length > 0) {
                for (let course of courseTable[id]) {
                    if (ders_isimleri.toString().includes(getCourseId(course))) {
                        return x;
                    }
                }
            }
        }
    }
    console.log("Error : 001");
    return -1;
}


// cakisma yaratmayan tek subeli dersleri otomatik olarak sec
// her bir yeni bir ders seciminden sonra otomatik calisir
function cakismasizDersleriSec() {
    let key = true;


    // - kutuda bir tane secilebilir ders olması gerek
    // - hic bir derlse cakismayan tek subeli olmali


    // adım adım tum dersleri kontrol et
    // course cakismaz secilebilir bir ders mi
    for (let course of allCourses) {
        key = true;
        // coursun secilebilir olması gerek
        if (!secilebilirDers(course)) {
            continue;
        }


        for (let box of courseTable) {
            // kutuda course ve ecilebilir baska dersler var
            // bu yuzden course secilemez
            if (box.includes(course) && box.filter(i => secilebilirDers(i)).length > 1) {
                key = false;
                break;
            }

            for (let course2 of box) {
                // 2 farki secilebilir sube tespiti
                if (course !== course2 && getCourseId(course) === getCourseId(course2) && secilebilirDers(course) && secilebilirDers(course2)) {
                    key = false;
                    break;
                }
            }
            if (key === false)
                break;
        }

        if (key) {
            console.log("otomatik secilio :", course);
            secilen_dersler.push(course);
            selectedCourseNames.push(getCourseId(course))
            tum_optionlari_yenile();
        }
    }
}


function secilebilirDers(course) {
    return !selectedCourseNames.includes(getCourseId(course)) && !secilmeyen_dersler.includes(course);
}




function updateAkts() {
    let x = document.getElementById("aktsBox");
    x.innerHTML = "akts: " + getTotalAkts();
}


// ders sildikten sonra klan son dersi bulmana gerek yok
function ders_sil(ders) {
    console.log("siliniyor : ", ders);

    secilen_dersler.splice(secilen_dersler.indexOf(ders), 1);
    selectedCourseNames.splice(selectedCourseNames.indexOf(getCourseId(ders)), 1);
    secilmeyen_dersler = [];
    tum_optionlari_yenile();

}


function getRandom() {
    return Math.floor(Math.random() * (10000 - 1000)) + 1000;
}


function options_list_clear(id) {

    while (document.getElementById(id).getElementsByTagName("option").length > 0) {
        document.getElementById(id).getElementsByTagName("option").item(0).remove();
    }
}


function tum_optionlari_yenile() {

    let repeat = false;

    for (let id = 0; id <= getLastCourseFinishTime() * 5 + 4; id++) {
        let box = courseTable[id];
        for (let course of box) {
            if (secilen_dersler.some(i => i.split("_")[0] === course.split("_")[0])) { // türevi veya kendisi secilmis ders,
                if (secilen_dersler.includes(course)) {  // kendisi secilmis ise diger tum dersleri devre sisi birak
                    for (let i of box) {
                        if (!secilen_dersler.includes(i) && !secilmeyen_dersler.includes(i)) {
                            secilmeyen_dersler.push(i);
                        }
                    }
                    break;
                } else if (!secilmeyen_dersler.includes(course)) { // turevi secildigi icin tekrar secemessi
                    secilmeyen_dersler.push(course);
                    repeat = true;
                }
            }
        }
    }

    if (repeat === true) {
        tum_optionlari_yenile();
        return;
    }


    let tmp;

    for (let id = 0; id <= getLastCourseFinishTime() * 5 + 4; id++) {
        tmp = courseTable   [id];

        // hic ders yok
        if (tmp.length === 0) {
            document.getElementById(id).getElementsByTagName("select").item(0).style.visibility = "hidden";
            continue;
        }


        let yeni_tmp = [];
        let secilen_var = false;
        let secilen_ders = "";

        options_list_clear(id);
        let x = document.getElementById(id).getElementsByTagName("select").item(0);
        x.removeAttribute("style");
        x.removeAttribute("disabled");


        //secilen ve secilebilir ders listesi kontrol
        for (let i of tmp) {

            if (secilebilirDers(i)) { // secilmemis bir ders ve engelsiz
                yeni_tmp.push(i);
            }
            if (secilen_dersler.includes(i)) {
                secilen_ders = i;
                secilen_var = true;
                yeni_tmp.push(i);
            }
        }

        if (yeni_tmp.length === 1 && secilen_var) {

            x.add(newOption(id, yeni_tmp[0]));
            x.title = courseDB.getCourseName(yeni_tmp[0].split("_")[0]);
            x.style.backgroundColor = courseDB.getCourseColor(yeni_tmp[0].split("_")[0]);

        } else if (yeni_tmp.length === 0) {
            x.disabled = "disabled";
        } else if (yeni_tmp.length > 0) {
            x.add(newOption(id, "ders seç"))
            x.title = "";
            for (let i of yeni_tmp) {
                x.add(newOption(id, i));
            }
        }
    }
    updateAkts();
    rightSide();
}

function newOption(id, m) {
    id = id + "_" + getRandom();
    let option = document.createElement("option");
    option.text = m;
    option.id = id;
    option.title = courseDB.getCourseName(id);
    return option;
}


function getTotalAkts() {
    let total = 0;
    secilen_dersler.forEach(course => {
        let courseId = course.split("_")[0];
        total += Number(courseDB.getCourseAkts(courseId));
    });
    return total;

}





let selectElements = document.querySelectorAll('.mySelect');

// Her bir select elementi için change olayı dinleyicisi ekle
selectElements.forEach(function(selectElement) {
    selectElement.addEventListener('mousedown', function(event) {
        let course = event.target.value;

        // remove course
        if (secilen_dersler.includes(course)) {
            ders_sil(event.target.value);
            event.preventDefault(); // Varsayılan davranışı engelle
        }
    });

    selectElement.addEventListener('change', function(event) {
        let selectedOption = event.target.options[event.target.selectedIndex];

        const course = selectedOption.text;

        if (allCourses.includes(course)) {

            // remove course for firefox
            if (secilen_dersler.includes(course)) {

                ders_sil(event.target.value);
                console.log(event.target.value);
            }


            // select course
            else {

                //    alert("t: " + select.closest('td').innerText);

                secilen_dersler.push(course);
                selectedCourseNames.push(getCourseId(course));


                tum_optionlari_yenile();
                cakismasizDersleriSec();
                rightSide()
            }
        }

    });
});

